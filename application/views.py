from application import app
from application.controllers import admin_post_controller
from application.controllers import site_controller
from application.controllers import error_controller

if __name__ == '__main__':
    # app.run(port=8080)  # run the built in application server
    # or
    app.run(port=8080, debug=True)
