from flask import Flask, session
from flask.ext.session import Session
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)  # global flask object
sess = Session()
app.config['SQLALCHEMY_DATABASE_URI'] = \
    'mysql+pymysql://root@localhost/flask_blog'

app.secret_key = '_$e@M8t=t-Xk@o3jXmb<oJFp<K`nLH'
app.config['SESSION_TYPE'] = 'filesystem'
sess.init_app(app)

db = SQLAlchemy(app)
from application import views
