from flask import render_template
from application import app, db
from application.model import *


@app.route('/404')
def pageNotFound():
    return "خطای 404 :  صفحه مورد نظر یافت نشد."


@app.route('/403')
def accessDenied():
    return "خطای 403 : شما دسترسی لازم برای مشاهده این صفحه را ندارید."


@app.route('/500')
def serverError():
    return "خطای 500 : مشکلی در سرور وجود دارد. لطفا مجددا تلاش کنید."
