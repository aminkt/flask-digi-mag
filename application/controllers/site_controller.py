from flask import render_template
from application import app, db
from application.model import *

categories = db.session.query(Category).all()


@app.route('/')
@app.route('/index')
def index():
    posts = db.session.query(Post).limit(5).all()
    return render_template('index.html', posts=posts, categories=categories)


@app.route('/about')
def about():
    return render_template('about.html', categories=categories)


@app.route('/post/<postId>')
def showPost(postId):
    post = Post.query.get(postId)
    return render_template('post.html', post=post, categories=categories)
