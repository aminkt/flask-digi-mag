from flask import render_template, flash, redirect, url_for, request
from application import app, db
from application.model import *


@app.route('/admin/posts')
def listPosts():
    posts = db.session.query(Post).all()
    return render_template('admin/post/post_list.html', posts=posts)


@app.route('/admin/posts/create', methods=['GET', 'POST'])
@app.route('/admin/posts/update/<postId>', methods=['GET', 'POST'])
def addPost(postId=None):
    if postId:
        post = Post.query.get(postId)
    else:
        post = None

    if request.method == "POST":
        if request.form['post'] == '' or request.form['post'] is None:
            return redirect('pageNotFound')
        else:
            title = request.form['title']
            content = request.form['content']
            thumb = request.form['thump']
            category = request.form['category']
            author = 1
            tags = request.form['tags']

            if postId is None:
                p = Post(title, author, content, tags, category, thumb)
                db.session.add(p)
                db.session.commit()
                flash('پست مورد نظر با موفقیت ایجاد شد.')
                return redirect(url_for('listPosts'))
            else:
                post.title = title
                post.content = content
                post.thumb = thumb
                post.category = category
                post.author = author
                post.tags = tags
                db.session.commit()
                flash('پست مورد نظر با موفقیت ویرایش شد.')
                return redirect('addPost', postId=postId)

    categories = db.session.query(Category).all()
    return render_template('admin/post/post_add.html', post=post, categories=categories)


@app.route('/admin/posts/delete/<postId>')
def deletePost(postId):
    Post.query.filter(Post.id == postId).delete()
    db.session.commit()
    flash('پست مورد نظر با موفقیت حذف شد.')
    return redirect(url_for('listPosts'))
