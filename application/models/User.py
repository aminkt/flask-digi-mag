import time

import jdatetime

from application import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), nullable=False)
    password = db.Column(db.String(120), nullable=False)
    name = db.Column(db.Text, nullable=True)
    family = db.Column(db.String(250), nullable=True)
    createAt = db.Column(db.DateTime, nullable=False)

    posts = db.relationship("Post", back_populates="user")

    def __init__(self, username, password, name, family, create_at=None):
        self.username = username
        self.password = password
        self.name = name
        self.family = family
        if create_at is None:
            self.createAt = time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            self.createAt = create_at

    def __str__(self):
        return str(self.id) + ' ' + str(self.title)

    def getFullName(self):
        return self.name + ' ' + self.family

    def getJdate(self):
        timestamp = time.mktime(self.createAt.timetuple())
        return jdatetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d ساعت %H:%M')
