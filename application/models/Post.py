import time

import jdatetime
from flask import url_for

from application import db


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    thump = db.Column(db.String(250), nullable=True)
    authorId = db.Column(db.Integer, db.ForeignKey('user.id'))
    content = db.Column(db.Text, nullable=True)
    tags = db.Column(db.String(250), nullable=True)
    categoryId = db.Column(db.Integer, db.ForeignKey('category.id'))
    createAt = db.Column(db.DateTime, nullable=False)

    user = db.relationship("User", back_populates="posts")
    category = db.relationship("Category", back_populates="posts")

    def __init__(self, title, author_id, content, tags, category_id, thumb=None, create_at=None):
        self.title = title
        self.authorId = author_id
        self.content = content
        self.tags = tags
        self.categoryId = category_id
        self.thump = thumb
        if create_at is None:
            self.createAt = time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            self.createAt = create_at

    def getUrl(self):
        return url_for('showPost', postId=self.id)

    def getThumb(self):
        if self.thump:
            return self.thump
        else:
            return url_for('static', filename='img/image_not_found.jpg')

    def getJdate(self):
        timestamp = time.mktime(self.createAt.timetuple())
        return jdatetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d ساعت %H:%M')

    def getTagsArray(self):
        return self.tags.split(",")
