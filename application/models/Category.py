import time

import jdatetime

from application import db


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    createAt = db.Column(db.DateTime, nullable=False)

    posts = db.relationship("Post", back_populates="category")

    def __init__(self, name, create_at=None):
        self.name = name
        if create_at is None:
            self.createAt = time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            self.createAt = create_at

    def getJdate(self):
        timestamp = time.mktime(self.createAt.timetuple())
        return jdatetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d ساعت %H:%M')
