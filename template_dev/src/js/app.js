function openMenu() {
    let menu = document.querySelector('.menu-container');
    menu.classList.add('active');

    let menuBtn = document.querySelector('.btn-menu');
    menuBtn.setAttribute('onclick', 'closeMenu();');
    menuBtn.innerHTML = '<i class="fa fa-times"></i>';
}

function closeMenu() {
    let menu = document.querySelector('.menu-container');
    menu.classList.remove('active');

    let menuBtn = document.querySelector('.btn-menu');
    menuBtn.setAttribute('onclick', 'openMenu();');
    menuBtn.innerHTML = '<i class="fa fa-bars"></i>';
}

function openModal() {
    let modal = document.getElementById('mymodal');
    // console.log()
    let btn = document.getElementById("modalBtn");
    let span = document.getElementsByClassName("close")[0];
    btn.onclick = function () {
        // console.log("hh");
        modal.style.display = "block";
        modal.style.transition = "all 5s;"
    };
    span.onclick = function () {
        modal.style.display = "none";
    };
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}