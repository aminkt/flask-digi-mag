from flask_script import Manager
from application import db, app
from application.model import *

manager = Manager(app)


def buildUser():
    u = User(
        username='admin',
        password='password',
        name='Amin',
        family='Keshavarz'
    )

    db.session.add(u)
    db.session.commit()


def buildCategory():
    c = Category(
        name='بدون دسته'
    )
    db.session.add(c)
    db.session.commit()

    c = Category(
        name='اطلاعیه ها'
    )
    db.session.add(c)
    db.session.commit()

    c = Category(
        name='برنامه کلاس ها'
    )
    db.session.add(c)
    db.session.commit()

    c = Category(
        name='اسلاید دروس'
    )
    db.session.add(c)
    db.session.commit()


def buildPosts():
    posts = []

    p = Post('Post 1', 1, 'Contetnt of post 1', 'tag1', 1)
    db.session.add(p)
    posts.append(p)

    p = Post('Post 2', 1, 'Contetnt of post 2', 'tag1', 1)
    db.session.add(p)
    posts.append(p)

    p = Post('Post 3', 1, 'Contetnt of post 3', 'tag1=3', 1)
    db.session.add(p)
    posts.append(p)

    db.session.commit()
    return posts


@manager.command
def initdb():
    """ Create database """
    db.create_all()
    buildUser()
    buildCategory()
    posts = buildPosts()
    print('Successful')


@manager.command
def dropall():
    """ Drop database """
    db.drop_all()
    print('Successful')


if __name__ == '__main__':
    manager.run()
